# -*- ecoding: utf-8 -*-
# @ModuleName: test_stock_attention.py
# @Author: lianghaodong
# @Time: 2023/6/20 15:13
import logging

from common.appium_dirver import  driver
from pageobjects.lhd.login_page import LoginPage
from pageobjects.lhd.remark_page import RemarkPage
from pagelocations.lhd import remark_page_loc as p
from utils.data_util import yaml_datas_dir

class TestRemark:

    def setup(self):
        self.lp=LoginPage(driver)
        self.rp=RemarkPage(driver)

    def teardown(self):
        self.rp.exit()

    def test_remark(self):
        self.lp.to_login_page()
        nickname=self.lp.do_login('15235896287', 'lhd15235896287')

        self.rp.to_stock_dynamic()
        result=self.rp.do_remark()

        act_nickname = ""  # 实际结果 昵称默认为空
        act_result = 0  # 默认0-找不到昵称
        try:
            # 昵称
            nc = self.rp.loc_elements(p.nickNameView)
            act_result = 1
        except:
            act_result = 0
            logging.info("截图")
            self.rp.save_screen('test_remark_error.png')

        # 定义一个空列表 后期存放
        act_nickname = []
        if act_result == 1:
            for i in nc:
                # 文本追加到列表
                act_nickname.append(i.text)
        # 断言评论前后数量
        assert result[0]<result[1]
        # 断言登录用户名与评论用户名一致
        assert nickname[1]==act_nickname[1]
        # 去评论列表
        self.rp.to_remark_list()