# -*- ecoding: utf-8 -*-
# @ModuleName: test_stock_attention.py
# @Author: lianghaodong
# @Time: 2023/6/20 15:13
import logging

from common.appium_dirver import  driver
from pageobjects.lhd.login_page import LoginPage
from pageobjects.lhd.attention_page import AttentionPage


class TestAttention:

    def setup(self):
        self.lp=LoginPage(driver)
        self.ap=AttentionPage(driver)

    def teardown(self):
        self.ap.exit()

    def test_attention(self):
        self.lp.to_login_page()
        logging.debug("登录成功后打印关注数量")
        num=self.lp.do_login('15235896287','lhd15235896287')
        print(num)
        self.ap.to_stock_dynamic()
        logging.debug("打印关注用户名")
        nickname=self.ap.do_attention()
        print(nickname)
        logging.debug("关注数量+1,关注列表包含刚刚关注用户")
        result=self.ap.to_attention_list()
        print(result[0])
        print(result[1])
        if num[0]<result[0]:
            print("关注成功")
            assert nickname == result[1]
        else:
            print("取消关注成功")
            assert nickname != result[1]
        if num[0]==result[0]:
            self.ap.save_screen('test_stock_attention_error.png')