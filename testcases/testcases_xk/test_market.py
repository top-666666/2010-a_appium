#-*- coding:utf-8 -*-
"""
作者: 邢凯
日期: 2023/6/20 15:54
@File: test_market.py
"""
import logging
from pageobjects.login_page import LoginPage
from common.appium_dirver import driver
from pageobjects.pageobjects_xk.market_page import MarketPage


class TestLogin:

    def setup_class(self):
        self.lp = LoginPage(driver)
        self.ml=MarketPage(driver)

    def teardown_class(self):
        self.ml.exit()

    def test_login(self):
        logging.info("appium启动成功")
        self.lp.to_login_page()
        self.lp.do_login('15131059453', 'XK011211...')
        self.ml.do_markt()
