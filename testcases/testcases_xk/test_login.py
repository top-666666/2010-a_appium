'''
-*- coding:utf-8 -*-
作者: 邢凯
日期: 2023/6/19 21:27
@File: test_login.py
'''

import logging
from pageobjects.login_page import LoginPage
from common.appium_dirver import driver


class TestLogin:

    def setup_class(self):
        self.lp = LoginPage(driver)

    def teardown_class(self):
        self.lp.exit()

    def test_login(self):
        logging.info("appium启动成功")
        self.lp.to_login_page()
        self.lp.do_login('15131059453', 'XK011211...')