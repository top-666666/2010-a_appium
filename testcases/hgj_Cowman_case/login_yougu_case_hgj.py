from common.appium_dirver import driver
from pageobjects.login_page import LoginPage

import logging

class TestLogin:
    def setup(self):
        self.po=LoginPage(driver)

    def teardown(self):
        self.po.driver.quit()

    def test_login(self):
        logging.info("appium启动成功")
        self.po.to_login_page()
        self.po.do_login('19917342109','3517718851Aa')