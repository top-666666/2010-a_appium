from pagelocations.hgj_Cowman_page.login_yougu_page_hgj import PageCowman
from common.appium_dirver import driver
from pageobjects.login_page import LoginPage

import logging

class TestLogin:
    def setup(self):
        self.po = LoginPage(driver)
        self.po1=PageCowman(driver)


    def teardown(self):
        self.po.exit()


    def test_login(self):

        try:
            logging.info("appium启动成功")
            self.po.to_login_page()
            self.po.do_login('19917342109','3517718851Aa')
            m=self.po1.concern_cowman()

            if m[0]>=m[4]:
                logging.info("关注数量断言错误")
                self.po.save_screen('count.png')
            assert m[0]<m[4]

            if m[1]==m[3]:
                logging.info("关注字体断言错误")
                self.po.save_screen('ziti.png')
            assert m[1] !=m[3]

            if m[2]!=m[5]:
                logging.info("用户名称断言错误")
                self.po.save_screen('name.png')
            assert m[2]==m[5]
        except:
            logging.info("出错了")
            self.po.save_screen('6.20 .png')



