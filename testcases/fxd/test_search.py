# ----*-----coding:utf-8 ------*--------
"""
@author : 冯祥弟
@FileName : test_search
@time : 2023/6/23 21:17
"""
import logging
from pageobjects.login_page import LoginPage
from common.appium_dirver import driver
from pageobjects.fxd.search import Testsearch
from pagelocations.fxd.search_page import Scarch

class TestLogin:

    def setup_class(self):
        self.po = LoginPage(driver)
        self.op=Testsearch(driver)
        self.sd = Scarch()

    def teardown_class(self):
        self.po.exit()

    def test_search(self):
        self.po.to_login_page()
        self.po.do_login('18538998211','123456aa')
        self.op.test_search()

        name = 'test'
        dy = self.po.loc_element(self.sd.ele_dy).test

        if name == dy:
            try :
                logging.info('断言成功')
                assert  name in dy

            except:
                logging.info('断言错误')
                self.po.save_screen('sousuo.png')

            count = 4
            dy = self.po.loc_elements(self.sd.ele_dy)
            if count != dy:
                try:
                    logging.info('断言成功')
                    assert  count ==len(dy)

                except:
                    logging.info('断言错误')
                    self.po.save_screen('sousuo.png')

