import logging
from pageobjects.login_page import LoginPage
from common.appium_dirver import driver
import time


class TestLogin:

    def setup_class(self):
        self.lp = LoginPage(driver)

    def teardown_class(self):
        self.lp.exit()

    def test_login(self):
        logging.info("appium启动成功")
        self.lp.to_login_page()
        self.lp.do_login('13403537030','LY431126964')

