import logging
import time
from pageobjects.login_page import LoginPage
from common.appium_dirver import driver
from pageobjects.pageobjects_ly.match_page import MatchPage


class TestLogin:

    def setup_class(self):
        self.lp = LoginPage(driver)
        self.ma=MatchPage(driver)

    def teardown_class(self):
        self.lp.exit()

    def test_login(self):
        logging.info("appium启动成功")
        self.lp.to_login_page()
        self.lp.do_login('15131059453', 'XK011211...')
        stime = time.strftime('%Y%m%d-%H%M%S', time.localtime())
        name=self.ma.add_match(stime)
        if name==stime:
            logging.info('断言成功')
        else:
            logging.info('断言失败')
        assert name==stime
