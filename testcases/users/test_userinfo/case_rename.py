#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：2010-a_appium 
@File    ：case_rename.py
@IDE     ：PyCharm 
@Author  ：张桑
@Date    ：2023/6/20 17:00 
"""
import time

import pytest
from common.appium_dirver import driver
from common.base_page import BasePage
from pageobjects.login_page import LoginPage
from pageobjects.users.userinfo_page.accessing_user_page import AccessingUserPage


class Test:
    def test_rename(self):
        LoginPage(driver).arrival_login()
        LoginPage(driver).do_login(username='17334292134', password='Aa1945054')
        time.sleep(3)
        AccessingUserPage(driver).accessing_user()
