# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/20 17:12
@Author : 李佳豪
@File   : test_login.py
"""
from common.appium_dirver import driver
from pageobjects.discover_module.login_page import LoginPage


class TestLogin:

    def test_login(self):
        username = '17351633931'
        password = 'LJH20011124'

        LoginPage(driver).to_login_page()
        LoginPage(driver).do_login(username, password)


