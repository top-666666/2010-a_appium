# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/27 10:22
@Author : 李佳豪
@File   : test_search.py
"""
import logging

from common.appium_dirver import driver
from pageobjects.discover_module.search_page import SearchPage


class TestSearch:

    def test_search(self, search_data):
        content = search_data['content']
        except_result = search_data['yq']  # 1有相关内容 0无相关内容
        sp = SearchPage(driver)
        assert_list = sp.do_search(content)
        # 获取到的文本
        text_result = assert_list['text_result']
        # 实际结果
        act_result = assert_list['act_result']

        # 断言是否有相关内容
        try:
            logging.info("预期结果："+str(except_result))
            logging.info("实际结果："+str(act_result))
            assert except_result == act_result
            logging.info("测试通过，预期结果实际结果匹配~~~")
        except:
            logging.error("测试失败,预期结果实际结果不匹配!")
            sp.save_screen('assert_failed')
            raise AssertionError("断言 是否有相关内容错误")

        # 断言 预期无相关内容，提示'抱歉，没有搜索到相关内容'
        if act_result == 0:
            try:
                assert text_result == '抱歉，没有搜索到相关内容'
                logging.info("测试通过，提示'抱歉，没有搜索到相关内容'匹配正确~~~")
            except:
                logging.error("测试失败，预期无相关内容，提示信息不正确!")
                sp.save_screen('assert_failed')
                raise AssertionError("断言 无相关内容的提示信息错误")

        # 断言 预期有相关内容时，关键在包含在牛人名字前面
        if act_result == 1:
            nickname_list = text_result
            try:
                for i in nickname_list:
                    assert content == i[:len(content)]
                logging.info("测试通过，用户昵称关键字匹配正确~~~")
            except:
                logging.error("测试失败，关键字匹配需求错误!")
                sp.save_screen('assert_failed')
                raise AssertionError("断言 关键字匹配需求错误")
