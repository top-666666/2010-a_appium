# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/27 15:28
@Author : 李佳豪
@File   : conftest.py
"""

import pytest

from utils.data_util import DataUtil
from common.appium_dirver import driver
from config.constants import app_package, app_activity


# 每个用例执行完，自动回到首页


@pytest.fixture(scope="function", autouse=True)
def activity_to_home():
    driver.start_activity(app_package, app_activity)


datas = DataUtil().read_yaml_tuple('superman_search_data.yaml')
print(datas)


@pytest.fixture(scope='function', params=datas)
def search_data(request):
    yield request.param
