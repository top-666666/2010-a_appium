# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/19 20:01
@Author : 李佳豪
@File   : first_to_home_page_loc.py
"""
from appium.webdriver.common.appiumby import AppiumBy

# 即刻启程
btn_tip_next = (AppiumBy.ID, 'com.jhss.youguu:id/btn_tip_next')
# 两次同意权限
permission_allow_button = (AppiumBy.ID, 'com.android.packageinstaller:id/permission_allow_button')
