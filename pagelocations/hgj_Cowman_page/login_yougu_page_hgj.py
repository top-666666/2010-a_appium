from appium import webdriver
from appium.webdriver.common.appiumby import AppiumBy
from common.base_page import BasePage
from time import sleep


class PageCowman(BasePage):
    # 提取断言关注数
    count = (AppiumBy.ID, 'com.jhss.youguu:id/tv_focus_num')


    #进入关注页面
    guanzhu_page=(AppiumBy.ID,'com.jhss.youguu:id/btn_focus')



    #取消关注
    Cancel_guanzhu=(AppiumBy.ID,'com.jhss.youguu:id/commontAttText')

    #返回按钮到我的页面
    back=(AppiumBy.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.ImageButton')


    #定位返回按钮返回到首页
    concern=(AppiumBy.ID,'com.jhss.youguu:id/btn_back')



    #进入牛人模块
    cowman=(AppiumBy.ID,'com.jhss.youguu:id/btn_desktop_superman')

    #关注推荐牛人用户(同时也是第一次关注和已关注的断言)
    cowman_guanzhu=(AppiumBy.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.TextView[1]')

    #提取推荐牛人的名称
    name_cowman=(AppiumBy.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.TextView[2]')

    #点击完关注后变为已关注取文本值的定位
    guanzhunext=(AppiumBy.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.TextView[1]')


    #进入我的页面
    me_page=(AppiumBy.ID,'com.jhss.youguu:id/head_pic')

    #提取关注断言值(点击进入关注页面)
    count1=(AppiumBy.ID,'com.jhss.youguu:id/tv_focus_num')

    #提取关注牛人的名称(多元素定位)
    name1_cowman=(AppiumBy.ID,'com.jhss.youguu:id/nickNameView')


    def concern_cowman(self):
        sleep(3)



        #进入关注页面
        self.loc_element(self.guanzhu_page).click()
        sleep(2)

        #取消关注
        self.loc_element(self.Cancel_guanzhu).click()
        sleep(2)

        #返回我的页面
        self.loc_element(self.back).click()
        sleep(2)

        self.swipe_direction('down',300)
        sleep(2)

        # 提取关注数
        n = self.loc_element(self.count).text
        sleep(2)

        #返回到我的页面
        self.loc_element(self.concern).click()
        sleep(2)

        #进入牛人模块
        self.loc_element(self.cowman).click()
        sleep(2)

        #关注断言
        n1=self.loc_element(self.cowman_guanzhu).text
        sleep(2)

        #定位名称断言值
        n2=self.loc_element(self.name_cowman).text
        sleep(2)

        #关注推荐牛人
        self.loc_element(self.cowman_guanzhu).click()
        sleep(2)

        #关注后断言
        n3=self.loc_element(self.guanzhunext).text
        sleep(2)


        #进入我的页面
        self.loc_element(self.me_page).click()
        sleep(2)

        #刷新页面
        self.swipe_direction('down',300)
        sleep(2)

        #提取关注断言数量
        n4=self.loc_element(self.count1).text
        sleep(2)

        #关注页面
        self.loc_element(self.count1).click()
        sleep(2)

        #断言名称
        n5=self.loc_element(self.name1_cowman).text
        sleep(2)


        return [n,n1,n2,n3,n4,n5]










