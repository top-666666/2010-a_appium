#-*- coding:utf-8 -*-
"""
作者: 邢凯
日期: 2023/6/20 15:24
@File: match_page_loc.py
"""
from selenium.webdriver.common.by import By
#点击返回按钮
btn_back=(By.ID,"com.jhss.youguu:id/btn_back")
#点击行情
btn_desktop_market=(By.ID,"com.jhss.youguu:id/btn_desktop_market")
#定位沪深
hushen=(By.XPATH,'//android.support.v7.app.ActionBar.Tab[@content-desc="沪深"]/android.widget.TextView')
#点击热门行业  国防军工
tv_stock_classify=(By.ID,"com.jhss.youguu:id/tv_stock_classify")
#点击股票
stock=(By.ID,"com.jhss.youguu:id/name")
#点击自选
btn_create_chat=(By.XPATH,"com.jhss.youguu:id/btn_create_chat")
#点击下一个
iv_page_change_right_arrow=(By.ID,"com.jhss.youguu:id/iv_page_change_right_arrow")
#获取股票名称
tv_title=(By.ID,"com.jhss.youguu:id/tv_title")
#点击持股牛人头像
img=(By.XPATH,"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[5]/android.view.View/android.view.View[1]/android.widget.Image")
#定位关注
bt_send_bull=(By.ID,"com.jhss.youguu:id/bt_send_bull")