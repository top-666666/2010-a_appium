#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：2010-a_appium 
@File    ：accessing_userpage_loc.py
@IDE     ：PyCharm 
@Author  ：张桑
@Date    ：2023/6/21 11:35 
"""
from appium.webdriver.common.appiumby import AppiumBy

'''定位昵称框'''
name = (AppiumBy.ID,'com.jhss.youguu:id/person_info_tv_nickname')
'''定位昵称编辑框'''
edit_name = (AppiumBy.ID,'com.jhss.youguu:id/alert_nick_nametxt')
