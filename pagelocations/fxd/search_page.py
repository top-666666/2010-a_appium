# ----*-----coding:utf-8 ------*--------
"""
@author : 冯祥弟
@FileName : search_page
@time : 2023/6/23 21:07
"""
from appium.webdriver.common.appiumby import AppiumBy
class Scarch():
    # 点击返回按钮
    ele_back = (AppiumBy.ID, "com.jhss.youguu:id/btn_back")
    # 搜索框
    ele_ss = (AppiumBy.ID,'com.jhss.youguu:id/ll_fake_search_btn')
    # 搜索内容
    ele_nr = (AppiumBy.ID,'com.jhss.youguu:id/et_stock_search')
    # 断言
    ele_dy = (AppiumBy.ID,'com.jhss.youguu:id/nickNameView')
