from selenium.webdriver.common.by import By
#点击返回按钮
btn_back=(By.ID,"com.jhss.youguu:id/btn_back")
#点击模拟炒股
btn_desktop_market=(By.XPATH,"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.ImageView[2]")
#点击比赛
match=(By.XPATH,'//android.widget.TextView[@text="比赛"]')
#输入比赛名称
et_match_name=(By.ID,"com.jhss.youguu:id/et_match_name")
#输入比赛简介
et_match_introduce=(By.ID,"com.jhss.youguu:id/et_match_introduce")
#初始资金
tv_initial_fund=(By.ID,"com.jhss.youguu:id/tv_initial_fund")
#10万
price=(By.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.GridView/android.widget.RelativeLayout[1]')
#点击创建
btn_right_text=(By.ID,"com.jhss.youguu:id/btn_right_text")
#点击确定
btn_common_confirm=(By.ID,"com.jhss.youguu:id/btn_common_confirm")
#点击取消
btn_common_cancel=(By.ID,"com.jhss.youguu:id/btn_common_cancel")
#点击我创建的
mycreate=(By.XPATH,'//android.widget.TextView[@text="我创建的"]')
#比赛名称
tv_match_name=(By.ID,"com.jhss.youguu:id/tv_match_name")



