# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/20 20:34
@Author : 李佳豪
@File   : superman_page_loc.py
"""
from appium.webdriver.common.appiumby import AppiumBy

# 点击【查看更多】
tv_change_superman = (AppiumBy.ID, 'com.jhss.youguu:id/tv_change_superman')
# 选择榜单[1:6]
TextView_ranking_list = (AppiumBy.CLASS_NAME, 'android.widget.TextView')
# 选择用户[8::13]
# TextView_username_list = (AppiumBy.CLASS_NAME, 'android.view.View')
TextView_username_list = (AppiumBy.XPATH, '//android.view.View[@text!=""]')




# 获取牛人用户名
tv_superman_name_01 = (AppiumBy.ID, 'com.jhss.youguu:id/tv_superman_name_01')
# 获取成功率文本
tv_superman_num_01 = (AppiumBy.ID, 'com.jhss.youguu:id/tv_superman_num_01')
# 获取榜单名
tv_superman_des_rank_01 = (AppiumBy.ID, 'com.jhss.youguu:id/tv_superman_des_rank_01')


