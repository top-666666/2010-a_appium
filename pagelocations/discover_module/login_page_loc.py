# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/13 15:11
@Author : 李佳豪
@File   : login_page.py
"""
from appium.webdriver.common.appiumby import AppiumBy

# 头像登录
head_pic = (AppiumBy.ID, 'com.jhss.youguu:id/head_pic')
# 进入登录
btn_login = (AppiumBy.ID, 'com.jhss.youguu:id/btn_login')
# 选择账号密码登录
tv_change_login_type = (AppiumBy.ID, 'com.jhss.youguu:id/tv_change_login_type')
# 输入用户名
et_username = (AppiumBy.ID, 'com.jhss.youguu:id/et_username')
# 输入密码
et_password = (AppiumBy.ID, 'com.jhss.youguu:id/et_password')
# 点击登录按钮
bt_login = (AppiumBy.ID, 'com.jhss.youguu:id/bt_login')
# 定位发现
btn_desktop_discovery = (AppiumBy.ID, 'com.jhss.youguu:id/btn_desktop_discovery')
