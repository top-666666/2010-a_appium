# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/27 9:30
@Author : 李佳豪
@File   : search_page_loc.py
"""
from appium.webdriver.common.appiumby import AppiumBy

# 点击搜索框
ll_fake_search_btn = (AppiumBy.ID, 'com.jhss.youguu:id/ll_fake_search_btn')
# 输入搜索内容
et_stock_search = (AppiumBy.ID, 'com.jhss.youguu:id/et_stock_search')
# 点击到牛人页面[0]
superman_page = (AppiumBy.ANDROID_UIAUTOMATOR, 'new UiSelector().text("牛人")')
# 获取昵称[0]
nickNameView = (AppiumBy.ID, 'com.jhss.youguu:id/nickNameView')
# 获取搜索不到元素的文本提示
tv_tips = (AppiumBy.ID, 'com.jhss.youguu:id/tv_tips')



