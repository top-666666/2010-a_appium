# -*- ecoding: utf-8 -*-
# @ModuleName: attention_page_loc.py
# @Author: lianghaodong
# @Time: 2023/6/20 10:02

from appium.webdriver.common.appiumby import AppiumBy

# 聊股内容
tv_content=(AppiumBy.ID,'com.jhss.youguu:id/tv_content')

# 关注人
attention_uname=(AppiumBy.ID,'com.jhss.youguu:id/nickNameView')

# 关注按钮
iv_wbc_follow=(AppiumBy.ID,'com.jhss.youguu:id/iv_wbc_follow')

# 返回
btn_back=(AppiumBy.CLASS_NAME,'android.widget.ImageButton')

# 发现
btn_desktop_discovery=(AppiumBy.ID,'com.jhss.youguu:id/btn_desktop_discovery')

# 头像
head_pic = (AppiumBy.ID, 'com.jhss.youguu:id/head_pic')

# 关注数量
tv_focus_num=(AppiumBy.ID,'com.jhss.youguu:id/tv_focus_num')

# 关注列表
btn_focus=(AppiumBy.ID,'com.jhss.youguu:id/btn_focus')

# 关注用户
nickNameView=(AppiumBy.ID,'com.jhss.youguu:id/nickNameView')