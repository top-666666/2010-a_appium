# -*- ecoding: utf-8 -*-
# @ModuleName: remark_page_loc.py
# @Author: lianghaodong
# @Time: 2023/6/20 19:16

from appium.webdriver.common.appiumby import AppiumBy

# 聊股内容
tv_content=(AppiumBy.ID,'com.jhss.youguu:id/tv_content')

# 写评论
commentBtn=(AppiumBy.ID,'com.jhss.youguu:id/commentBtn')

# 评论编辑框
edit_content=(AppiumBy.ID,'com.jhss.youguu:id/edit_content')

# 发布
title_right_button=(AppiumBy.ID,'com.jhss.youguu:id/title_right_button')

# 评论数量
comment_count=(AppiumBy.ID,'com.jhss.youguu:id/comment_count')

# 评论人
nickNameView=(AppiumBy.ID,'com.jhss.youguu:id/nickNameView')

# 返回
btn_back=(AppiumBy.CLASS_NAME,'android.widget.ImageButton')

# 发现
btn_desktop_discovery=(AppiumBy.ID,'com.jhss.youguu:id/btn_desktop_discovery')

# 头像
head_pic = (AppiumBy.ID, 'com.jhss.youguu:id/head_pic')

# 聊股
btn_weibo=(AppiumBy.ID,'com.jhss.youguu:id/btn_weibo')

# 我的评论
content=(AppiumBy.XPATH,'//android.support.v7.app.ActionBar.Tab[@content-desc="我的评论"]')