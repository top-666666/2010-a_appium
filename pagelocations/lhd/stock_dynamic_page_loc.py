# -*- ecoding: utf-8 -*-
# @ModuleName: stock_dynamic_page_loc.py
# @Author: lianghaodong
# @Time: 2023/6/20 9:13

from appium.webdriver.common.appiumby import AppiumBy

# 返回
btn_back=(AppiumBy.ID,'com.jhss.youguu:id/btn_back')

# 聊股
btn_desktop_talk=(AppiumBy.ID,'com.jhss.youguu:id/btn_desktop_talk')
