# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/15 9:50
@Author : 李佳豪
@File   : constants.py
"""
from common import handler_path

# 每个用例执行完后，自动返回首页
app_package = 'com.jhss.youguu'
app_activity = 'com.jhss.youguu.ui.DesktopActivity'


# 调试设备的宽和高
WIDTH = 720  # 宽
HEIGHT = 1280  # 高

# 邮件信息
e_sender = ''  # 发件人
e_password = ''  # 密码
e_host = 'smtp.qq.com'  # 邮件类型，默认为qq
e_receiver = '收件人'  # 收件人
e_subject = '主题'  # 主题
e_contents = '内容'  # 内容
e_attachments = ['', '']  # 附件 例如：attachments = ['handler_path.logs_dir', 'handler_path.reports_dir']
'''
各个附件地址：
日志：handler_path.logs_dir
报告：handler_path.reports_dir
截图：handler_path.screenshots_dir
'''

# 报告信息配置(已经配置好了allure报告，无需使用此报告)
r_filename = 'result.html'  # 文件名
r_suite = None  # suite套件配置
r_title = '测试报告'  # 标题
r_blacklist = ['']  # 黑名单
r_whitelist = None  # 白名单
r_description = ''  # 描述
r_tester = ''  # 测试人
'''
报告保存在outputs/reports下
白/黑名单只可以使用一个
'''

