# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/16 10:00
@Author : 李佳豪
@File   : appium_dirver.py
"""
import yaml
from appium import webdriver
from common.handler_path import driver_dir


class AppiumDriver:
    def __init__(self, prot=4723, **kwargs):
        """
        yaml重构
        return: driver对象
        """
        # 读取yaml内容
        with open(driver_dir, encoding='utf-8') as f:
            desired_caps = yaml.load(f, Loader=yaml.FullLoader)
        # print(desired_caps)

        # 增加其他参数
        for key, value in kwargs.items():
            desired_caps[key] = value
        # 启动会话
        self.driver = webdriver.Remote(command_executor=f'http://127.0.0.1:{prot}/wd/hub',
                                       desired_capabilities=desired_caps)
        self.driver.implicitly_wait(10)


driver = AppiumDriver().driver
