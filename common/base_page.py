# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/15 9:13
@Author : 李佳豪
@File   : base_page.py
"""
import logging
import os
import time

from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.support.ui import WebDriverWait
from common.handler_path import screenshots_dir

from config import constants


class BasePage:

    def __init__(self, driver: webdriver.Remote):
        self.driver = driver

    def loc_element(self, loc):
        """ 获取单个元素 """
        try:
            return self.driver.find_element(*loc)
        except:
            logging.error(f"未定位到元素{loc}")
            # 失败截图
            self.save_screen("loc_element")
            raise

    def loc_elements(self, loc):
        """ 获取多个元素 """
        try:
            return self.driver.find_elements(*loc)
        except:
            logging.error(f"未定位到元素{loc}")
            # 失败截图
            self.save_screen("loc_elements")
            raise

    def click_element(self, loc):
        """ 单元素点击 """
        try:
            self.loc_element(loc).click()
        except:
            logging.error(f"未定位到元素{loc}")
            # 失败截图
            self.save_screen("click_element")
            raise

    def click_elements(self, loc, index):
        """
        多元素点击其中一个元素
        loc:元素位置
        index:所在的列表下标
        """
        try:
            self.loc_elements(loc)[index].click()
        except:
            logging.error(f"未定位到元素{loc}")
            # 失败截图
            self.save_screen("click_elements")
            raise

    def send_keys_element(self, loc, content):
        """ 单元素输入内容 """
        try:
            self.loc_element(loc).send_keys(content)
        except:
            logging.error(f"无法输入内容{loc}")
            # 失败截图
            self.save_screen("send_keys_element")
            raise

    def send_keys_elements(self, loc, index, content):
        """
        多元素点击
        loc:元素位置
        index:所在的列表下标
        content: 输入的内容
        """
        try:
            self.loc_elements(loc)[index].send_keys(content)
        except:
            logging.error(f"无法输入内容{loc}")
            # 失败截图
            self.save_screen("send_keys_elements")
            raise

    def loc_wait_element(self, loc, timeout, interval=0.5):
        """
        显示等待
        loc:元素位置
        timeout:持续时间
        interval:查询的频率，默认为间隔0.5s
        """
        try:
            return WebDriverWait(self.driver, timeout, interval).until(lambda x: x.find.element(*loc))
        except:
            logging.error(f"无法定位到元素{loc}")
            # 失败截图
            self.save_screen("loc_wait_element")
            raise

    def tap_el(self, x, y):
        """
        轻敲
        x:横坐标
        y:纵坐标
        """
        try:
            # 某一台设备的宽和高
            w = constants.WIDTH
            h = constants.HEIGHT
            # 计算比例
            x_bili = x / w
            y_bili = y / h
            # 通过比例计算当前设备的绝对坐标
            new_w = self.driver.get_window_size()['width']
            new_h = self.driver.get_window_size()['height']
            # 最终点击的坐标new x，new y
            new_x = x_bili * new_w
            new_y = y_bili * new_h
            # 执行点击操作
            self.driver.tap([(new_x, new_y)])
        except:
            logging.error(f"点击失败坐标：({x},{y})")
            # 失败截图
            self.save_screen("tap_el")
            raise

    def loc_swipe(self, x1, y1, x2, y2,frequency=1,duration=None):
        """
        滑动
        x1 y1:滑动开始的坐标
        x2 y2:滑动结束的坐标
        frequency:滑动的次数 默认为 1
        duration:持续时间 默认为None
        """
        try:
            # 某一台设备的宽和高
            w = constants.WIDTH
            h = constants.HEIGHT
            # 计算比例
            x1_per = x1 / w
            y1_per = y1 / h
            x2_per = x2 / w
            y2_per = y2 / h
            # 获取当前设备的宽高
            new_w = self.driver.get_window_size()['width']
            new_h = self.driver.get_window_size()['height']
            new_x1 = x1_per * new_w
            new_y1 = y1_per * new_h
            new_x2 = x2_per * new_w
            new_y2 = y2_per * new_h
            for i in range(frequency):
                self.driver.swipe(new_x1, new_y1, new_x2, new_y2, duration)
        except:
            logging.error(f"滑动失败坐标：({x1},{y1}),{x2},{y2})")
            # 失败截图
            self.save_screen("loc_swipe")
            raise

    def swipe_direction(self, direction, duration=None):
        """
        滑动（简单版）
        direction：
            left:左滑
            right:右滑
            on：上滑
            down：下滑
        duration:持续时间 默认为None
        """
        try:
            w = self.driver.get_window_size()['width']
            h = self.driver.get_window_size()['height']
            # 左滑
            if direction == "left":
                self.driver.swipe(w * 0.8, h * 0.5, w * 0.2, h * 0.5, duration)
            # 右滑
            if direction == "right":
                self.driver.swipe(w * 0.2, h * 0.5, w * 0.8, h * 0.5, duration)
            # 上滑
            if direction == "on":
                self.driver.swipe(w * 0.5, h * 0.8, w * 0.5, h * 0.2, duration)
            # 下滑
            if direction == "down":
                self.driver.swipe(w * 0.5, h * 0.2, w * 0.5, h * 0.8, duration)
        except:
            logging.error(f"滑动失败,方式为：{direction}")
            # 失败截图
            self.save_screen("loc_swipe")
            raise

    def loc_scroll(self, ele1, ele2, duration=None):
        """
        滚动
        ele1:元素1
        ele2:元素2
        duration:持续时间 默认为None
        """
        try:
            self.driver.scroll(ele1, ele2, duration)
        except:
            logging.error("滚动失败")
            # 失败截图
            self.save_screen("loc_scroll")
            raise

    def loc_drag_and_drop(self, ele1, ele2):
        """
        拖拽
        ele1:元素1
        ele2:元素2
        """
        try:
            self.driver.drag_and_drop(ele1, ele2)
        except:
            logging.error("拖拽失败")
            # 失败截图
            self.save_screen("loc_drag_and_drop")
            raise

    def touch_long_press(self, ele=None, x=None, y=None, duration=1000):
        """
        长按
        ele: 元素位置（默认None）
        x,y: 坐标（默认None）
        ele与 x,y 选一个
        duration: 持续时间 默认为1000ms
        """
        try:
            TouchAction(self.driver).long_press(ele, x, y, duration=duration).perform()
        except:
            logging.error("长按失败")
            # 失败截图
            self.save_screen("touch_long_press")
            raise

    def touch_double_tap(self, ele=None, x=None, y=None, count=1):
        """
        多次点击
        x,y: 坐标（默认None）
        ele与 x,y 选一个
        count: 点击次数 默认为1次
        """
        try:
            TouchAction(self.driver).tap(ele, x, y, count).perform()
        except:
            logging.error(f"多次点击失败，点击次数：{count}")
            # 失败截图
            self.save_screen("touch_double_tap")
            raise

    def save_screen(self, picture_name):
        """
        截图
        picture_name:图片的名称

        """
        stime = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime())
        filepath = os.path.join(screenshots_dir, '{}_{}.png'.format(picture_name, stime))
        self.driver.save_screenshot(filepath)
        logging.info("图片保存在：{}".format(filepath))

    def exit(self):
        self.driver.quit()
