# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/27 9:54
@Author : 李佳豪
@File   : search_page.py
"""
import logging
import time

from common.base_page import BasePage
from pagelocations.discover_module import search_page_loc as spl


class SearchPage(BasePage):

    def do_search(self, content):
        # 点击搜索框
        self.click_element(spl.ll_fake_search_btn)
        # 输入搜索内容
        self.send_keys_element(spl.et_stock_search, content)
        # 点击到牛人页面[0]
        self.click_elements(spl.superman_page, 0)

        def get_nickname_list():
            nick_name_list = []
            count = 0
            before_flag_name = None
            while True:
                if count == 0:
                    ele_list = self.loc_elements(spl.nickNameView)
                    before_flag_name = ele_list[-1].text
                    logging.info("获取到的昵称为：" + before_flag_name)
                    for i in ele_list:
                        nick_name_list.append(i.text)
                    count = count + 1
                else:
                    time.sleep(2)
                    self.swipe_direction("on", 500)
                    time.sleep(2)
                    self.swipe_direction("on", 500)
                    ele_list = self.loc_elements(spl.nickNameView)
                    after_flag_name = ele_list[-1].text
                    logging.info("获取到的昵称为：" + after_flag_name)
                    if after_flag_name == before_flag_name:
                        break
                    else:
                        before_flag_name = after_flag_name
                    for i in ele_list:
                        nick_name_list.append(i.text)
            logging.info('名称列表：' + str(nick_name_list))
            return nick_name_list

        try:
            time.sleep(2)
            # 无相关内容的文本提示
            text_result = self.loc_element(spl.tv_tips).text
            logging.info("无相关内容的文本提示：" + text_result)
            act_result = 0
        except:
            # 有相关内容获取昵称
            act_result = 1
            text_result = get_nickname_list()

        return {
            "text_result": text_result,
            "act_result": act_result
        }
