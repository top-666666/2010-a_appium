# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/21 9:04
@Author : 李佳豪
@File   : superman_page.py
"""
import logging
import time

from common.base_page import BasePage
from pagelocations.discover_module import superman_page_loc as spl


class SupermanPage(BasePage):

    def superman_info(self):
        # 获取牛人用户名
        superman_name = self.loc_element(spl.tv_superman_name_01).text
        logging.info("牛人姓名：" + superman_name)
        # 获取成功率文本
        success_possibly = self.loc_element(spl.tv_superman_num_01).text
        logging.info("成功率：" + success_possibly)
        # 获取榜单名
        ele = self.loc_element(spl.tv_superman_des_rank_01).text
        i = 0
        while True:
            if ele[i] == "榜":
                break
            i = i + 1
        order_list_name = ele[0:i + 1]
        logging.info("牛人所在榜单：" + order_list_name)
        # 点击【查看更多】
        self.click_element(spl.tv_change_superman)
        time.sleep(3)
        # 获取榜单[1:6]
        rank_list = self.loc_elements(spl.TextView_ranking_list)[1:6]
        # 选择该牛人的榜单
        index = 0
        order_name = None
        while index <= 5:
            if rank_list[index].text == order_list_name:
                order_name = rank_list[index].text
                logging.info("选择的榜单为：" + order_name)
                break
            index = index + 1
        # 点击该榜单
        self.click_elements(spl.TextView_ranking_list, index + 1)
        time.sleep(5)

        if not order_name:
            logging.error("未匹配到榜单名")
            raise ValueError("未匹配到榜单名")
        if order_name == '总盈利榜':
            self.profit_ranking_list(superman_name)

    def profit_ranking_list(self, username):
        # 获取榜单所有用户
        username_list = self.loc_elements(spl.TextView_username_list)[::9]
        success_list = self.loc_elements(spl.TextView_username_list)[3::9]
        username_text_list = []
        success_text_list = []
        for i, j in zip(username_list, success_list):
            username_text_list.append(i.text)
            success_text_list.append(j.text)
        logging.info("用户名包括：" + str(username_text_list))
        logging.info("用户成功率包括：" + str(success_text_list))
        # 判断是否有该用户
        flag = 0
        while True:
            flag = flag + 1
            if username not in username_text_list:
                self.loc_swipe(150, 1113, 150, 194, 500)
                # 获取榜单所有用户
                username_list = self.loc_elements(spl.TextView_username_list)[::9]
                success_list = self.loc_elements(spl.TextView_username_list)[3::9]
                username_text_list = []
                success_text_list = []
                for i, j in zip(username_list, success_list):
                    username_text_list.append(i.text)
                    success_text_list.append(j.text)
                logging.info("用户名包括：" + str(username_text_list))
                logging.info("用户成功率包括：" + str(success_text_list))
            else:
                break

            if flag == 6:
                logging.error('不包含该用户名')
                break
