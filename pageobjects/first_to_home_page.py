# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/13 15:11
@Author : 李佳豪
@File   : login_page.py
"""
import time
from time import sleep

from appium.webdriver.common.appiumby import AppiumBy
from common.base_page import BasePage
from pagelocations import first_to_home_page_loc as fthpl


class FirstToHomePage(BasePage):

    def first_to_home(self):
        # 左滑三次
        for i in range(3):
            self.swipe_direction("left", 500)
            time.sleep(1)
        # 即刻启程
        self.loc_element(fthpl.btn_tip_next).click()
        # 两次同意权限
        self.loc_element(fthpl.permission_allow_button).click()
        self.loc_element(fthpl.permission_allow_button).click()

