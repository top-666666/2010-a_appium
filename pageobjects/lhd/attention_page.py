# -*- coding: utf-8 -*-
# @ModuleName: attention_page.py
# @Author: lianghaodong
# @Time: 2023/6/20 15:10

from time import sleep

from common.base_page import BasePage
from pagelocations.lhd import stock_dynamic_page_loc as stp
from pagelocations.lhd import attention_page_loc as ap


class AttentionPage(BasePage):

    # 去动态页
    def to_stock_dynamic(self):
        self.loc_element(stp.btn_back).click()
        self.loc_element(stp.btn_desktop_talk).click()
        sleep(2)

    # 执行关注
    def do_attention(self):
        self.loc_element(ap.tv_content).click()
        self.tap_el(500,500)
        nickname=self.loc_element(ap.attention_uname).text
        self.loc_element(ap.iv_wbc_follow).click()
        return nickname

    # 去关注列表
    def to_attention_list(self):
        self.loc_element(ap.btn_back).click()
        self.loc_element(ap.btn_desktop_discovery).click()
        self.loc_element(ap.head_pic).click()
        sleep(2)
        result=self.loc_element(ap.tv_focus_num).text
        self.loc_element(ap.btn_focus).click()
        nickname=self.loc_elements(ap.nickNameView)[0].text
        return result,nickname