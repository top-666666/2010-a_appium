# -*- coding: utf-8 -*-
# @ModuleName: remark_page.py
# @Author:lianghaodong
# @Time: 2023/6/20 19:24

from time import sleep

from common.base_page import BasePage
from pagelocations.lhd import stock_dynamic_page_loc as stp
from pagelocations.lhd import remark_page_loc as rp


class RemarkPage(BasePage):

    # 去动态页
    def to_stock_dynamic(self):
        self.loc_element(stp.btn_back).click()
        self.loc_element(stp.btn_desktop_talk).click()
        sleep(2)

    # 执行评论
    def do_remark(self):
        self.loc_element(rp.tv_content).click()
        count_front = self.loc_element(rp.comment_count).text
        self.tap_el(500,500)
        self.loc_element(rp.commentBtn).click()
        self.loc_element(rp.edit_content).send_keys('test')
        self.loc_element(rp.title_right_button).click()
        sleep(2)
        count_after=self.loc_element(rp.comment_count).text
        return count_front,count_after

    # 去评论列表
    def to_remark_list(self):
        self.loc_element(rp.btn_back).click()
        self.loc_element(rp.btn_desktop_discovery).click()
        self.loc_element(rp.head_pic).click()
        sleep(2)
        self.loc_element(rp.btn_weibo).click()
        self.loc_element(rp.content).click()
        sleep(2)