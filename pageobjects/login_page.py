# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/13 15:11
@Author : 李佳豪
@File   : login_page.py
"""
import time
from time import sleep

from appium.webdriver.common.appiumby import AppiumBy
from common.base_page import BasePage
from pagelocations import login_page_loc as lpl


class LoginPage(BasePage):

    def to_login_page(self):
        # # 左滑三次
        # for i in range(3):
        #     self.swipe_direction("left", 500)
        # # 即刻启程
        # self.loc_element(lpl.btn_tip_next).click()
        # # 两次同意权限
        # self.loc_element(lpl.permission_allow_button).click()
        # self.loc_element(lpl.permission_allow_button).click()
        # 点击发现
        self.loc_element(lpl.btn_desktop_discovery).click()
        # 头像登录
        self.loc_element(lpl.head_pic).click()
        # 进入登录
        self.loc_element(lpl.btn_login).click()
        sleep(3)

    # 登录账号(账号密码登录)
    def do_login(self, username, password):
        # 选择账号密码登录
        self.loc_element(lpl.tv_change_login_type).click()
        # 输入用户名
        self.loc_element(lpl.et_username).send_keys(username)
        # 输入密码
        self.loc_element(lpl.et_password).send_keys(password)
        # 点击登录按
        self.loc_element(lpl.bt_login).click()

    # 进入登录界面
    def arrival_login(self):
        """点击左上角头像"""
        self.tap_el(57,73)
        '''点击登录按钮'''
        self.click_element(lpl.btn_login)
