# ----*-----coding:utf-8 ------*--------
"""
@author : 冯祥弟
@FileName : search
@time : 2023/6/23 21:12
"""

from time import sleep

from common.base_page import BasePage
from pagelocations.fxd.search_page import Scarch


class Testsearch(BasePage):
    def test_search(self):
        self.loc_element(Scarch.ele_back).click()

        self.loc_element(Scarch.ele_ss).click()
        sleep(2)
        self.loc_element(Scarch.ele_nr).send_keys('test')


