#-*- coding:utf-8 -*-
"""
作者: 邢凯
日期: 2023/6/20 15:50
@File: match_page.py
"""
from time import sleep

from common.base_page import BasePage
from pagelocations.pagelocations_xk import market_page_loc as mpl

class MarketPage(BasePage):
    def do_markt(self):
        #点击返回按钮
        self.loc_element(mpl.btn_back).click()
        #点击行情
        self.loc_element(mpl.btn_desktop_market).click()
        # 点击沪深
        self.loc_element(mpl.hushen).click()
        # 点击热门行业  国防军工
        self.loc_element(mpl.tv_stock_classify).click()
        # 点击股票
        self.loc_element(mpl.stock).click()
        #轻敲屏幕
        self.tap_el(630,1225)
        sleep(2)
        #获取股票标题
        stock_name=self.loc_element(mpl.tv_title).text
        print(stock_name)
        # # 点击更多
        self.tap_el(300, 1230)
        # # self.loc_element(mpl.btn_create_chat).click()
        # #点击持股牛人
        self.tap_el(300, 1158)
        #点击持股牛人头像
        self.loc_element(mpl.img).click()
        #点击关注
        self.loc_element(mpl.bt_send_bull).click()
        sleep(5)

