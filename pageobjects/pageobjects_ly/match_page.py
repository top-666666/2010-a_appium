from time import sleep
import time
from common.base_page import BasePage
from pagelocations.pagelocations_ly import match_page_loc as ll

class MatchPage(BasePage):
    def add_match(self,stime):
        #点击返回按钮
        self.click_element(ll.btn_back)
        #点击模拟炒股
        self.click_element(ll.btn_desktop_market)
        # 点击比赛
        self.click_element(ll.match)
        #创建比赛
        self.tap_el(817,1259)
        sleep(2)
        #输入比赛名称
        self.send_keys_element(ll.et_match_name,stime)
        #输入比赛简介
        self.send_keys_element(ll.et_match_introduce,stime)
        #初始资金
        self.click_element(ll.tv_initial_fund)
        #10万
        self.click_element(ll.price)
        #点击创建
        self.click_element(ll.btn_right_text)
        # 点击确定
        self.click_element(ll.btn_common_confirm)
        # 点击取消
        self.click_element(ll.btn_common_cancel)
        #我创建的
        self.click_element(ll.mycreate)
        #比赛名称
        name=self.loc_element(ll.tv_match_name).text

        return name