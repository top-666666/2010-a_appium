# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/15 11:31
@Author : 李佳豪
@File   : email_util.py
"""
import yagmail
from config.constants import e_sender, e_password, e_host, e_receiver, e_subject, e_contents, e_attachments

''' 
sender:发送邮件的账号
password:发送账号的image
receiver:接受的邮件账号
subject:主题
contents:内容
attachments:附件
'''


class EmailUtil:
    def set_email(self):
        email = yagmail.SMTP(user=e_sender, password=e_password, host=e_host)
        email.send(to=e_receiver, subject=e_subject, contents=e_contents, attachments=e_attachments)
