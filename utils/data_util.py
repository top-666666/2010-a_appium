import os

import yaml, csv, pandas as pd
# import xlrd
from common.handler_path import yaml_datas_dir, csv_datas_dir, xls_datas_dir


class DataUtil:

    # 读取yaml文件
    def read_yaml_tuple(self, filename):
        """ filename文件名 """
        filepath = os.path.join(yaml_datas_dir, filename)
        with open(filepath, 'r+', encoding='utf-8') as file:
            data = yaml.load(stream=file, Loader=yaml.FullLoader)
            return data

    # 返回形式({'content': '股神', 'yq': 1}, {'content': '哈哈哈', 'yq': 1}])

    # 读取csv文件，返回是元组（需要用 * 解除元祖）
    def read_csv_tuple(self, filename):
        """ filename文件名 """
        filepath = os.path.join(csv_datas_dir, filename)
        with open(filepath, encoding='utf-8') as file:
            data_list = csv.reader(file)
            i = 1
            list_data = []
            for row in data_list:
                if i > 1:
                    list_data.append(row)
                i = i + 1
            # print(tuple(list_data))
            return tuple(list_data)

    # 返回形式(['2428937359', 'LJH20011124'], ['2428937359', 'LJH20011'], ['', 'LJH20011124'])

    # 读取excel文件
    # def read_xlsx_list(self, filename, sheetname):
    #     """ filename文件名 sheetname表单名"""
    #     filepath = os.path.join(xls_datas_dir, filename)
    #     data = xlrd.open_workbook(filename)
    #     table = data.sheet_by_name(sheetname)
    #     list_data = []
    #     all_row = table.nrows
    #     for i in range(1, all_row - 1):
    #         row_values = table.row_values(i)
    #         list_data.append(row_values)
    #     print(tuple(list_data))
    #     return tuple(list_data)

    # 返回形式(['李佳豪', '男', 22.0, '2010A'], ['梁钰', '男', 20.0, '2010A'])

    # 读取excel文件（xlsx或xls）
    def read_xlsx_tuple(self, filename, sheet_name='Sheet1'):
        """ filename文件名 sheet_name表单名"""
        filepath = os.path.join(xls_datas_dir, filename)
        df = pd.read_excel(filepath, sheet_name=sheet_name)
        return tuple(df.values)
    # 返回形式(['李佳豪' 12345 '男' 18] ['hgj' 234 '女' 100])


if __name__ == '__main__':
    pass
    # 调试yaml
    # print(DataUtil().read_yaml_tuple('superman_search_data.yaml'))
    # 调试csv
    # print(DataUtil().read_csv_tuple('../datas/login_data.csv'))
    # print(DataUtil().read_xlsx_list(r'../datas/xls_data/login_data.xls', 'Sheet1'))
    # print(DataUtil().read_xlsx_tuple(r'demo.xlsx'))
