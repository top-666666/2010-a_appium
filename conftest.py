# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/20 18:54
@Author : 李佳豪
@File   : conftest.py
"""
import pytest
from common.appium_dirver import driver
from pageobjects.first_to_home_page import FirstToHomePage


# 项目里面执行一次
@pytest.fixture(scope="session", autouse=True)
def first_do_activity():
    FirstToHomePage(driver).first_to_home()
    yield
    driver.quit()
    