# -*- coding: utf-8 -*-
"""
@Time   : 2023/6/15 11:29
@Author : 李佳豪
@File   : main.py
"""
import os

import pytest

from utils import log_util

if __name__ == '__main__':
    # 使用日志： 请使用info或以上级别记录日志
    log_util.set_log("result.log")
    pytest.main(["./testcases",
                 "-sv", "--alluredir", "outputs/allure/temp_jsonreport", "--clean-alluredir"])
    os.system("allure generate outputs/allure/temp_jsonreport -o outputs/allure/html --clean")

